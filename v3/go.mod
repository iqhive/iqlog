module bitbucket.org/iqhive/iqlog/v3

go 1.23

require golang.org/x/crypto v0.26.0

require (
	golang.org/x/sys v0.23.0 // indirect
	golang.org/x/term v0.23.0 // indirect
)
